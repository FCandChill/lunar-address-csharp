﻿namespace LunarAddress
{
    public class AddressRAMZSNES
    {
        public static int PcToSnes(int PcAddress, out bool IsValidPcAddress)
        {
            int SnesAddress = 0;

            if (PcAddress < 0xC13 || PcAddress >= 0x20C13)
            {
                IsValidPcAddress = false;
            }
            else
            {
                IsValidPcAddress = true;
                SnesAddress = PcAddress - 0xC13 + 0x7E0000;

            }
            return SnesAddress;
        }
        public static int SnesToPc(int SnesAddress, out bool IsValidSnesAddress)
        {
            int PcAddress = 0;

            if (SnesAddress >= 0x7E0000 && SnesAddress <= 0x7FFFFF)
            {
                IsValidSnesAddress = true;
                PcAddress = (0x1FFFF & SnesAddress) + 0xC13;
            }
            else
            {
                IsValidSnesAddress = false;
            }

            return PcAddress;
        }
    }
}
