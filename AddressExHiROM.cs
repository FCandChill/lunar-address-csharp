﻿namespace LunarAddress
{
    public class AddressExHiROM
    {
        public static int PcToSnes(int PcAddress, out bool IsValidPcAddress)
        {
            int SnesAddress = 0;

            if (PcAddress >= 0x7E0000)
            {
                IsValidPcAddress = false;
            }
            else
            {
                IsValidPcAddress = true;

                SnesAddress = PcAddress;
                if (PcAddress < 0x400000)
                {
                    SnesAddress |= 0xC00000;
                }

                if (PcAddress >= 0x7E0000)
                {
                    SnesAddress -= 0x400000;
                }
            }
            return SnesAddress;
        }
        public static int SnesToPc(int SnesAddress, out bool IsValidSnesAddress)
        {
            int PcAddress = 0;
            if ((SnesAddress >= 0xC00000 && SnesAddress <= 0xFFFFFF) || (SnesAddress >= 0x400000 && SnesAddress <= 0x7DFFFF))
            {
                IsValidSnesAddress = true;
                PcAddress = SnesAddress & 0x3FFFFF;

                if (SnesAddress < 0xC00000)
                {
                    PcAddress += 0x400000;
                }
            }
            else
            {
                IsValidSnesAddress = false;
            }

            return PcAddress;
        }
    }
}
