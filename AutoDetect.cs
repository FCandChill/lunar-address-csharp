﻿using System.IO;
using System.Text;
using System.Windows.Forms;

namespace LunarAddress.Conversion
{
    public enum RomType
    {
        LoROM1 = 0,
        LoROM2 = 1,
        HiROM = 2,
        ExLoROM = 3,
        ExHiROM = 4,
        RAMZSNES = 5,
        VRAMZSNES = 6
    }

    public class AutoDetect
    {
        public static void Detect(byte[] bbb)
        {
            Stream s = new MemoryStream(bbb);

            byte ptr;

            using (BinaryReader b = new BinaryReader(s, Encoding.ASCII, true))
            {
                s.Position = 0x7FDC;

                if ((b.ReadUInt16() ^ b.ReadUInt16()) == 0xFFFF)
                {
                    s.Position = 0x7FD5;
                    ptr = b.ReadByte();
                }
                else
                {
                    s.Position = 0xFFDC;

                    if ((b.ReadUInt16() ^ b.ReadUInt16()) != 0xFFFF)
                    {
                        MessageBox.Show("Unknown ROM format...", "Cannot determine ROM memory map type.");
                        return;
                    }
                    s.Position = 0xFFD5;
                    ptr = b.ReadByte();
                }

                if ((ptr & 0xF) == 5)
                {
                    LunarRegistry.AddressConversion = RomType.ExHiROM;
                }
                else if ((ptr & 0xF) == 3)
                {
                    LunarRegistry.AddressConversion = RomType.HiROM;
                }
                else if ((ptr & 1) == 1)
                {
                    if (bbb.Length <= 0x400000)
                    {
                        LunarRegistry.AddressConversion = RomType.HiROM;
                    }
                    else
                    {
                        LunarRegistry.AddressConversion = RomType.ExHiROM;
                    }
                }
                else if (bbb.Length <= 0x400000)
                {
                    LunarRegistry.AddressConversion = (ptr >> 4) >= 3 ? RomType.LoROM2 : RomType.LoROM1;
                }
                else
                {
                    LunarRegistry.AddressConversion = RomType.ExLoROM;
                }
            }
        }
    }
}