# Lunar Address C# Libary

## About

Originally, when I was reverse engineering Lunar Address, I was remaking everything from code to UI. Per Klarth's suggestion, I made it multi-platform. I also figured it was pointless to remake Lunar Address's UI one to one because it was a bit more effort than it was worth and also people would complain if it wasn't 100% like it.

This repository includes non-UI assets from the incomplete C# version.
