﻿namespace LunarAddress
{
    public class AddressHiRom
    {
        public static int PcToSnes(int PcAddress, out bool IsValidPcAddress)
        {
            int SnesAddress = 0;

            if (PcAddress >= 0x400000)
            {
                IsValidPcAddress = false;
            }
            else
            {
                IsValidPcAddress = true;
                SnesAddress = PcAddress | 0xC00000;
            }
            return SnesAddress;
        }
        public static int SnesToPc(int SnesAddress, out bool IsValidSnesAddress)
        {
            int PcAddress = 0;
            if (SnesAddress >= 0xC00000 && SnesAddress <= 0xFFFFFF)
            {
                IsValidSnesAddress = true;
                PcAddress = (SnesAddress & 0x3FFFFF);
            }
            else
            {
                IsValidSnesAddress = false;
            }

            return PcAddress;
        }
    }
}
