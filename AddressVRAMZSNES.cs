﻿namespace LunarAddress
{
    public class AddressVRAMZSNES
    {
        public static int PcToSnes(int PcAddress, out bool IsValidPcAddress)
        {
            int SnesAddress = 0;

            if (!(PcAddress >= 0x20C13 && PcAddress < 0x30C13))
            {
                IsValidPcAddress = false;
            }
            else
            {
                IsValidPcAddress = true;
                SnesAddress = (PcAddress - 0x20C13) >> 1;
            }
            return SnesAddress;
        }
        public static int SnesToPc(int SnesAddress, out bool IsValidSnesAddress)
        {
            int PcAddress = 0;
            if (SnesAddress >= 0x0000 && SnesAddress <= 0x7fff)
            {
                PcAddress = (SnesAddress << 1) + 0x20C13;
                IsValidSnesAddress = true;
            }
            else
            {
                IsValidSnesAddress = false;
            }
            return PcAddress;
        }
    }
}
