﻿using Microsoft.Win32;

namespace LunarAddress
{
    public enum WindowSize
    {
        Small,
        Medium,
        Large,
        Expanded
    }

    public static class LunarRegistry
    {
        private static readonly string RegistryPath = @"HKEY_CURRENT_USER\Software\LunarianConcepts\LunarAddress\Settings\";
        private static readonly string RegistryLastConversion = "LastConversion";
        private static readonly string RegistryOptions = "Options";
        private static readonly string RegistryX = "X";
        private static readonly string RegistryY = "Y";
        public static bool Headered { get; set; }
        public static WindowSize WindowSize { get; set; }
        public static bool KeepOnTop { get; set; }
        public static bool WasLastConversionASNESAddress { get; set; }
        public static bool FormatResults { get; set; }
        public static bool PcOnLeft { get; set; }
        public static RomType AddressConversion { get; set; }

        public static string LastConversion
        {
            get => (string)Registry.GetValue(RegistryPath, RegistryLastConversion, "200");
            set => Registry.SetValue(RegistryPath, RegistryLastConversion, value);
        }
        public static int WindowXCoord
        {
            get => (int)Registry.GetValue(RegistryPath, RegistryX, -1);
            set => Registry.SetValue(RegistryPath, RegistryX, value);
        }
        public static int WindowYCoord
        {
            get => (int)Registry.GetValue(RegistryPath, RegistryY, -1);
            set => Registry.SetValue(RegistryPath, RegistryY, value);
        }

        /* 
         * xAAA xxww xxxx xxxx 1111 111L cfkh
         * 
         * 
         * h = headered checkbox
         * w = window size
         * k = keep on top
         * f = format results
         * c = was last conversion a snes address?
         * L = pc on left side
         * 1 = always 1.
         * x = always zero
         * A = address comversion
         */

        public static void ReadOptions()
        {
            int OptionByte = (int)Registry.GetValue(RegistryPath, RegistryOptions, 0);

            if (OptionByte != 0)
            {
                Headered = (OptionByte & 0x1) == 0x1;
                WindowSize = (WindowSize)(OptionByte & 0x300000 >> 20);
                KeepOnTop = ((OptionByte >> 1) & 0x1) == 0x1;
                FormatResults = ((OptionByte >> 2) & 0x1) == 0x1;
                WasLastConversionASNESAddress = ((OptionByte >> 3) & 0x1) == 0x1;
                PcOnLeft = ((OptionByte >> 4) & 0x1) == 0x1;
                AddressConversion = (RomType)((OptionByte & 0x7000000) >> 24);
            }
            else
            {
                Headered = true;
                WindowSize = WindowSize.Expanded;
                KeepOnTop = true;
                WasLastConversionASNESAddress = false;
                FormatResults = true;
                PcOnLeft = true;
                AddressConversion = RomType.LoROM1;
            }
        }
    }
}